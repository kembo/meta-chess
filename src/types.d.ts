export type PlayerLabel = "Player1" | "Player2";
export type BoardLabel = "A" | "B" | "C";
export type Direction = 0 | 1 | 2 | 3 | 4 | 5 | 6;
export type Position = [BoardLabel, Direction];
export type PieceProps = {
  readonly owner: PlayerLabel;
  readonly refBoard: BoardLabel;
};
export type BoardProps = {
  readonly label: BoardLabel;
  readonly hexes: Record<Direction, PieceProps | null>;
};
export type Game = {
  readonly boards: Readonly<Record<BoardLabel, BoardProps>>;
};
