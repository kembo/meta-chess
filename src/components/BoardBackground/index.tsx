import React from "react";
import { BoardBackgroundProps } from "./types";

const RADIUS = 300;
const RADIUS_HALF = RADIUS / 2;
const HEIGHT = RADIUS * 26 / 15;
const HEIGHT_HALF = HEIGHT / 2;
const HEX_POINTS: readonly [number, number][] = [
  [-RADIUS_HALF, -HEIGHT_HALF],
  [RADIUS_HALF, -HEIGHT_HALF],
  [RADIUS, 0],
  [RADIUS_HALF, HEIGHT_HALF],
  [-RADIUS_HALF, HEIGHT_HALF],
  [-RADIUS, 0],
];
const ARROW_HEIGHT = RADIUS / 10
const ARROW_WIDTH = RADIUS_HALF * 0.9
const ARROW_POINTS: readonly [number, number][] = [
  [0, 0],
  [ARROW_WIDTH, ARROW_HEIGHT],
  [0, ARROW_HEIGHT / 3],
  [-ARROW_WIDTH, ARROW_HEIGHT],
];

const BoardBackground = React.memo(
  function BoardBackground({ padding: rawPadding, ...opts }: BoardBackgroundProps) {
    function pathStr(orig: [number, number], pos: [number, number]): string {
      return [orig[0] + pos[0], orig[1] + pos[1]].join(",");
    }
    const boxWidth = RADIUS * 5;
    const boxHeight = HEIGHT * 3;
    return (
      <svg viewBox={`0 0 ${boxWidth} ${boxHeight}`} xmlns="http://www.w3.org/2000/svg" {...opts}>
        <polygon
          id="Hex" points={HEX_POINTS.map((v) => pathStr([RADIUS_HALF * 5, HEIGHT_HALF], v)).join(" ")}
          fill="none" stroke="black" strokeWidth={5}
        />
        <polygon
          id="Arrow" points={ARROW_POINTS.map((v) => pathStr([RADIUS_HALF * 5, 10], v)).join(" ")}
          fill="black" stroke="none"
        />
        {
          [1, 2, 3, 4, 5].map((i) => {
            const rotation = `rotate(${i * 60},${boxWidth / 2},${boxHeight / 2})`;
            return (
              <>
                <use xlinkHref="#Hex" transform={rotation} />
                <use xlinkHref="#Arrow" transform={rotation} />
              </>
            );
          })
        }
        <use
          xlinkHref="#Hex"
          transform={
            `translate(${[RADIUS_HALF * 5, HEIGHT_HALF * 3].join(",")}) scale(0.9)`
            + ` translate(${[-RADIUS_HALF * 5, -HEIGHT_HALF].join(",")})`
          }
        />
      </svg>
    )
  }
);
export default BoardBackground;
