import { ComponentProps } from "react";

type Position = [number, number];

export type BoardBackgroundProps = {
  readonly padding?: [number, number];
} & Omit<ComponentProps<"svg">, "padding" | "children" | "xmlns">
