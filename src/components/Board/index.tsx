import BoardBackground from '../BoardBackground';
import './styles.css';
import { BoardViewerProps } from './types';

export default function Board({ label }: BoardViewerProps) {
  return (
    <div className="Board">
      <div className="Label">
        <label>
          {label}
        </label>
      </div>
      <div className="Hex0"></div>
      <div className="Hex1"></div>
      <div className="Hex2"></div>
      <div className="Hex3"></div>
      <div className="Hex4"></div>
      <div className="Hex5"></div>
      <div className="Hex6"></div>
      <div className="Background">
        <BoardBackground />
      </div>
    </div>
  );
}
