import { BoardLabel } from "../../types";

export type BoardViewerProps = {
  label: BoardLabel;
};
